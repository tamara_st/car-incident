First you need to create database schema on your machine.
application.properties is file where you can set credentials (username, password) for you local database.
Application will generate tables in database during deployment, but you can do it executing script /scripts/carPart.sql.
Then you can put test data executing sql file testData.sql.
You can start application by running main function in class DemoApplication.
Then you can try this two calls using postman or some similar rest tool:

FIrst for creating incident:
    curl -X POST \
      http://localhost:8080/incident \
      -H 'Cache-Control: no-cache' \
      -H 'Content-Type: application/json' \
      -H 'Postman-Token: 3fecf46c-c412-4817-9faa-37c34fb43b2e' \
      -d '{

        "companyNumber": 1,
        "contractNumber": 1,
        "brokenCarParts": [
            {
                "carPartId": 1,
                "wageCost": 100.00,
                "materialCost": 180.00
            },

            {
                "carPartId": 2,
                "wageCost": 100.00,
                "materialCost": 30.00
            }
         ]
    }'

Second for getting incident by id:

curl -X GET \
  http://localhost:8080/incident/1 \
  -H 'Cache-Control: no-cache' \
  -H 'Postman-Token: 56aec9e7-5c8e-46fe-b871-a53d79e97471'

 //I did not have time for tests, so they are not present, after all 3 hours is a little bit short period of time. Sorry :)
