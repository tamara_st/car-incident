package gradimir.repository.impl;

import gradimir.persistance.CarPart;
import gradimir.persistance.Contract;
import org.springframework.stereotype.Repository;

@Repository
public class ContractDaoImpl extends GenericDAOImpl<Contract>{

    public Contract saveOrUpdateContract(Contract contact){
        if (contact.getId() == null) {
           return create(contact);
        }else{
            return update(contact);
        }
    }


    @Repository
    public static class CarPartDaoImpl extends GenericDAOImpl<CarPart>{

        public CarPart saveOrUpdateCarPart(CarPart carPart){
            if (carPart.getId() == null) {
               return create(carPart);
            }else{
                return update(carPart);
            }
        }


    }
}
