package gradimir.repository;


import gradimir.persistance.BaseEntity;
import java.util.List;

public interface CommonDAO {

	BaseEntity<?> findById(Class<?> cl, Long id);
	

}
