package gradimir.persistance;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "COUNTRY")
public class Country extends BaseEntity<Long>{

  @Column(name = "country_name")
  String countryName;

  @Builder
  public Country(Long id, Date created, Date updated, String countryName) {
    super(id, created, updated);
    this.countryName = countryName;
  }
}
