package gradimir.persistance;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "JOBS")
public class Job  extends BaseEntity<Long> {

  @Column(name = "job_name")
  private String jobName;

  @ManyToOne
  Individual individual;

  @Builder
  public Job(Long id, Date created, Date updated, String jobName, Individual individual) {
    super(id, created, updated);
    this.jobName = jobName;
    this.individual = individual;
  }
}
