package gradimir.persistance;

import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "INDIVIDUAL_ENTITY")
public class Individual extends BaseEntity<Long> {

  @OneToOne
  User user;

  @Column(name = "first_name")
  private String firstName;

  @Column(name = "last_name")
  private String lastName;

  @Column(name = "foreign_country_availability")
  private boolean foreignCountryAvailability;

  @Column(name = "area_availability")
  private Long areaAvailabilty;

  @Column(name = "new_interest")
  private boolean newInterest;

  @OneToMany(mappedBy = "individual", fetch = FetchType.EAGER)
  @Fetch(FetchMode.SELECT)
  List<Job> jobList;

  @OneToMany(mappedBy = "individual", fetch = FetchType.EAGER)
  @Fetch(FetchMode.SELECT)
  List<WorkExperience> workExperienceList;

  public Individual(Long id, Date created, Date updated,
      User user, String firstName, String lastName, boolean
      foreignCountryAvailability, Long areaAvailabilty, boolean newInterest,
      List<Job> jobList, List<WorkExperience> workExperienceList){
    super(id, created, updated);
    this.user = user;
    this.firstName = firstName;
    this.lastName = lastName;
    this.foreignCountryAvailability = foreignCountryAvailability;
    this.areaAvailabilty = areaAvailabilty;
    this.newInterest = newInterest;
    this.jobList = jobList;
    this.workExperienceList = workExperienceList;
  }


}
