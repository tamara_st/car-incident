package gradimir.persistance;


import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
//didn't work with @Builder here
@Table(name = "USER")
public class User extends BaseEntity<Long>{

    @Column(name = "username")
    private String username;

    @Column(name = "password")
    private String password;

    public User (Long id, Date created, Date updated, String username, String password) {
        super(id, created, updated);
        this.username = username;
        this.password = password;


    }
}