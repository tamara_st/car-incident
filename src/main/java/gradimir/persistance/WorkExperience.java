package gradimir.persistance;


import java.util.Date;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "WORK_EXPERIENCE")
public class WorkExperience extends BaseEntity<Long> {

  @ManyToOne
  Individual individual;

  @ManyToOne
  Job job;

  @Column(name="years_experience")
  public Integer yearsExperience;

  public WorkExperience(Long id, Date created, Date updated,
      Individual individual, Job job, Integer yearsExperience) {
    super(id, created, updated);
    this.individual = individual;
    this.job = job;
    this.yearsExperience = yearsExperience;
  }



}
