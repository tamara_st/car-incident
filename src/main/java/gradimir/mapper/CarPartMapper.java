package gradimir.mapper;


import gradimir.dto.CarPartRequestDto;
import gradimir.persistance.CarPart;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CarPartMapper {

        private CarPart mapCarPartDtoToCarPartPojo(CarPartRequestDto carPartRequestDto){
            return CarPart.builder()
                    .carPartName(carPartRequestDto.getCarPartName())
                    .build();
        }



}
