package gradimir.mapper;


import gradimir.dto.*;
import gradimir.persistance.Company;
import lombok.experimental.UtilityClass;

@UtilityClass
public class CompanyMapper {

    private Company mapCarPartDtoToCarPartPojo(CompanyRequestDto companyRequestDto){
        return Company.builder()
                .companyName(companyRequestDto.getCompanyName())
                .build();
    }


}
