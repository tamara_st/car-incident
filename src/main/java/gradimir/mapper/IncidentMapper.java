package gradimir.mapper;


import gradimir.dto.IncidentRequestDto;
import gradimir.dto.IncidentResponseDto;
import gradimir.persistance.*;
import gradimir.repository.impl.CompanyDaoImpl;
import gradimir.repository.impl.ContractDaoImpl;
import gradimir.repository.impl.ContractDaoImpl.CarPartDaoImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class IncidentMapper {

    @Autowired
    ContractDaoImpl contactDao;
    @Autowired
    CompanyDaoImpl companyDao;
    @Autowired
    CarPartDaoImpl carPartDao;


    public Incident getIncidentFromIncidentRequest(
        IncidentRequestDto incidentRequestDto) {
        Incident buildIncident = Incident.builder()
                .contact(contactDao.find(incidentRequestDto.getContractNumber()))
                .company(companyDao.find(incidentRequestDto.getCompanyNumber()))
                .build();

        List<PartIncidentPrices> partIncidentPricesList = new ArrayList<>();
        incidentRequestDto.getBrokenCarParts().forEach(irDto -> {
            partIncidentPricesList.add(PartIncidentPrices.builder()
                    .materialCost(irDto.getMaterialCost())
                    .wageCost(irDto.getWageCost())
                    .carPart(carPartDao.find(irDto.getCarPartId()))
                    .build()
            );
        });
        buildIncident.setPartIncidentPrices(partIncidentPricesList);
        return buildIncident;
    }

    public IncidentResponseDto getIncidentResponseDto(Incident incident) {
        return IncidentResponseDto.builder()
                .claimNumber(incident.getId())
                .contactNumber(incident.getContact().getId())
                .total(calculateSumForAllPartPrices(incident.getPartIncidentPrices()))
                .brokenCarParts(populateBrokenCarParts(incident.getPartIncidentPrices()))
                .build();
    }


    private Double calculateSumForAllPartPrices(List<PartIncidentPrices> partIncidentPrices) {
        Double sum = 0.0;
        for (PartIncidentPrices prices : partIncidentPrices) {
            sum += prices.getMaterialCost();
            sum += prices.getWageCost();
        }
        return sum;
    }

    private List<IncidentResponseDto.BrokenCarParts> populateBrokenCarParts(List<PartIncidentPrices> partIncidentPrices) {
        List<IncidentResponseDto.BrokenCarParts> brokenPartList = new ArrayList<>();
        partIncidentPrices.forEach(pip -> {
            brokenPartList.add(IncidentResponseDto.BrokenCarParts.builder()
                    .carPartName(pip.getCarPart().getCarPartName())
                    .cost(pip.getMaterialCost() + pip.getWageCost())
                    .build());
        });

        return brokenPartList;

    }


}
