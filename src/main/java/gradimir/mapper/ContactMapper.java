package gradimir.mapper;

import gradimir.dto.ContractRequestDto;
import gradimir.persistance.Contract;
import lombok.experimental.UtilityClass;

@UtilityClass
public class ContactMapper {

    private Contract mapCarPartDtoToCarPartPojo(
        ContractRequestDto contractRequestDto){
        return Contract.builder()
                .contactName(contractRequestDto.getContactName())
                .build();
    }


}
