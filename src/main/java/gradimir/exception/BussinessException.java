package gradimir.exception;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BussinessException extends RuntimeException {

  Object meta;
  public BussinessException(String message) {
    super(message);
  }



}
