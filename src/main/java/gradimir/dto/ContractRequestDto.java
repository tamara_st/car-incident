package gradimir.dto;

import lombok.Data;

@Data
public class ContractRequestDto {

    private Long contactNumber;
    private String contactName;


}
