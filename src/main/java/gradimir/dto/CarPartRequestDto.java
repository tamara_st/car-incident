package gradimir.dto;

import lombok.Data;

@Data
public class CarPartRequestDto {

    private Long carPartId;
    private String carPartName;


}
