package gradimir.service;


import gradimir.dto.IncidentRequestDto;
import gradimir.dto.IncidentResponseDto;

public interface IncidentService {

    public void saveCarPartIncident(IncidentRequestDto incidentRequestDto);
    public IncidentResponseDto getCarPartIncident(Long incidentId);


}
