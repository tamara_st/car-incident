package gradimir.service.impl;

import gradimir.dto.IncidentRequestDto;
import gradimir.dto.IncidentResponseDto;
import gradimir.exception.BussinessException;
import gradimir.mapper.IncidentMapper;
import gradimir.persistance.Incident;
import gradimir.repository.impl.CompanyDaoImpl;
import gradimir.repository.impl.ContractDaoImpl.CarPartDaoImpl;
import gradimir.repository.impl.IncidentDaoImpl;
import gradimir.service.IncidentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class IncidentServiceImpl implements IncidentService {


    @Autowired
    IncidentDaoImpl incidentDao;

    @Autowired
    CarPartDaoImpl carPartDao;

    @Autowired
    CompanyDaoImpl companyDao;

    @Autowired
    IncidentMapper incidentMapper;


    @Override
    @Transactional
    public void saveCarPartIncident(IncidentRequestDto incidentRequestDto) {
        Incident incidentFromIncidentRequest = incidentMapper.getIncidentFromIncidentRequest(incidentRequestDto);
        incidentDao.saveOrUpdateIncident(incidentFromIncidentRequest);
    }



    @Override
    public IncidentResponseDto getCarPartIncident(Long incidentId) {
       return incidentMapper.getIncidentResponseDto(
               Optional.ofNullable(incidentDao.find(incidentId))
                       .orElseThrow(() -> new BussinessException("Incident with id : " + incidentId + " does not exist.")));
    }
}
